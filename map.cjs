const items = require('./items.cjs');


function map(items, cb) {
    const result = [];
    if (!Array.isArray(items) || map.arguments.length < 2 || cb === undefined) {
        return [];
    }

    for(let index=0; index<items.length; index++){
        const eachElement = cb(items[index], index, items);
        result.push(eachElement);
    }
    return result;
}

module.exports = map;
