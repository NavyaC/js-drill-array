const items = require('./items.cjs');

function filter(items, cb) {
    const arr = [];

    if (!Array.isArray(items) || filter.arguments.length < 2) {
        return [];
    }

    for (let index = 0; index < items.length; index++) {
        const result = cb(items[index], index, items);

        if (result === true) {
            arr.push(items[index]);
        }
    }
    return arr;
}

module.exports = filter;
