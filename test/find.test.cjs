const items = require('../items.cjs');

const findFn = require('../find.cjs');


function cb(items) {
    return items > 3;
}

test("testing find problem", () => {
    expect(findFn(items, cb)).toEqual(items.find((value) => value > 3));
});
