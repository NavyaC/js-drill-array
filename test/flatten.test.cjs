const data = require('../data.cjs');

const flattenFn = require('../flatten.cjs');

test("Testing flatten", () => {
    expect(flattenFn(data,2)).toEqual(data.flat(2));
});
