const items = require('../items.cjs');

const filterFn = require('../filter.cjs');

function cb(items, index, array) {
    return items > 2;
}


test("Testing filter", () => {
    expect(filterFn(items, cb)).toEqual(items.filter((value, index, array) => value > 2));
});
