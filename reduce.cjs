const items = require('./items.cjs');

function reduce(items, cb, startingValue = items[0]) {
    if (!Array.isArray(items) || reduce.arguments.length < 2) {
        return [];
    }

    if (reduce.arguments.length === 2) {
        for (let index = 1; index < items.length; index++) {
            startingValue = cb(startingValue, items[index], index, items);
        }
    }
    else {
        for (let index = 0; index < items.length; index++) {
            startingValue = cb(startingValue, items[index], index, items);
        }
    }
    return startingValue;
}


module.exports = reduce;
