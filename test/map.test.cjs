const items = require('../items.cjs');
const mapFn = require('../map.cjs');

function cb(item, index, array) {
    return item;
}

test("Testing map", () => {
    expect(mapFn(items, cb)).toEqual(items.map((value, index, array) => value));
    expect(mapFn(items, parseInt)).toEqual(items.map(parseInt));
});
