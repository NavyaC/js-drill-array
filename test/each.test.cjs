cconst items = require('../items.cjs');
const eachFn = require('../each.cjs');

function cb(items) {
    return items;
}

test("testing each", () => {
    expect(eachFn(items)).toEqual([ 1, 2, 3, 4, 5, 5 ]);
});

