const data = require('./data.cjs');

function flatten(data, depth=1) {
    let result = [];
    depth = depth < 0 ? 0 : depth;
    
    if (!Array.isArray(data) || flatten.arguments.length < 1) {
        return [];
    }

    if (depth == 0)
    {
        for (let index=0; index<data.length; index++){
            if (data[index] !== undefined){
                result.push(data[index]);
            }
        }
    }
    else 
    {
        for (let index=0; index<data.length; index++){
            if (data[index] === undefined){
                continue;
            }
            if (Array.isArray(data[index]) && depth !== 0) {
                result.push(...flatten(data[index], depth - 1));
            }
            else {
                result.push(data[index]);
            }
        }
    }
    return result;
}
 
 

module.exports = flatten;
