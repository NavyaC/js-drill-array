const items = require('../items.cjs');

const reduceFn = require('../reduce.cjs');


function cb(acc, value, index, array) {
    return acc + value;
}

test("Testing reduce", () => {
    expect(reduceFn(items, cb, 0)).toEqual(items.reduce((acc, value, index, array) => acc + value, 0));
});
